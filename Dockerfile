#gatewayserver dockerfile


#Base image as openJdk tag in jre
FROM openjdk:8-jre

#copying the jar file into the image location /app
COPY target /app

#changing the working directory
WORKDIR /app

#specifying the executable for container to run
ENTRYPOINT ["java","-jar","Gateway-Service-0.0.1-SNAPSHOT.jar"]